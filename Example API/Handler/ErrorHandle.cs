﻿
using ExampleAPI.Models.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web;
using System.Web.Http.ExceptionHandling;
using System.Web.Http.Results;

namespace ExampleAPI.Handler
{
    public class ErrorHandler : ExceptionHandler
    {
        public Response<object> Response { get; set; }

        public override void Handle(ExceptionHandlerContext context)
        {
            // Handle Internal Server Error

            Response = new Response<object>();
            Response.SetStatus(HttpStatusCode.InternalServerError, context.Exception);

            context.Result = new ResponseMessageResult(new HttpResponseMessage(HttpStatusCode.InternalServerError)
            {
                Content = new StringContent(JsonConvert.SerializeObject(Response), Encoding.UTF8, "application/json")
            });
        }

        public override bool ShouldHandle(ExceptionHandlerContext context)
        {
            return true;
        }
    }
}