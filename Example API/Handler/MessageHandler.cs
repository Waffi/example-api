﻿using ExampleAPI.Models.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace ExampleAPI.Handler
{
    public class MessageHandler : DelegatingHandler
    {        
        public Response<object> Response { get; set; }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken)
        {
            HttpResponseMessage responseMessage = await base.SendAsync(request, cancellationToken);

            // Handle response format

            Response = new Response<object>();
            Response.SetStatus(responseMessage.StatusCode);

            if (responseMessage.IsSuccessStatusCode)
            {
                Response.Data = await responseMessage.Content.ReadAsAsync<object>();
            }

            responseMessage = new HttpResponseMessage(responseMessage.StatusCode)
            {
                Content = new StringContent(JsonConvert.SerializeObject(Response), Encoding.UTF8, "application/json")
            };

            return responseMessage;
        }
    }
}