﻿using ExampleAPI.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ExampleAPI.Connector
{
    public interface IServiceConnector
    {
        Task<Response<T>> GetAsync<T>(string requestUri);

        Task<Response<T>> PostAsync<T>(string requestUri, HttpContent content);
    }
}