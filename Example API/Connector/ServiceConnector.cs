﻿using ExampleAPI.Models.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace ExampleAPI.Connector
{
    public class ServiceConnector : IServiceConnector
    {
        private readonly HttpClient Client;

        public ServiceConnector(string baseUrl)
        {
            this.Client = new HttpClient();
            this.Client.BaseAddress = new Uri(baseUrl);
            this.Client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        }

        // Get Service

        public async Task<Response<T>> GetAsync<T>(string requestUri)
        {
            HttpResponseMessage responseMessage = await this.GetAsync<T>(requestUri, 3);

            return await responseMessage.Content.ReadAsAsync<Response<T>>();
        }

        private async Task<HttpResponseMessage> GetAsync<T>(string requestUri, int attempt)
        {
            HttpResponseMessage responseMessage = await Client.GetAsync(requestUri);

            if (responseMessage.IsSuccessStatusCode)
            {
                return responseMessage;
            }
            else
            {
                if (attempt != 0)
                {
                    return await this.GetAsync<T>(requestUri, attempt - 1);
                }
                else
                {
                    throw new HttpResponseException(responseMessage.StatusCode);
                }
            }
        }

        // Post Service

        public async Task<Response<T>> PostAsync<T>(string requestUri, HttpContent content)
        {
            HttpResponseMessage responseMessage = await this.PostAsync<T>(requestUri, content, 3);

            return await responseMessage.Content.ReadAsAsync<Response<T>>();
        }

        private async Task<HttpResponseMessage> PostAsync<T>(string requestUri, HttpContent content, int attempt)
        {
            HttpResponseMessage responseMessage = await Client.PostAsync(requestUri, content);

            if (responseMessage.IsSuccessStatusCode)
            {
                return responseMessage;
            }
            else
            {
                if (attempt != 0)
                {
                    return await this.PostAsync<T>(requestUri, content, attempt - 1);
                }
                else
                {
                    throw new HttpResponseException(responseMessage.StatusCode);
                }
            }
        }
    }
}