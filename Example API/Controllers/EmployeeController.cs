﻿using ExampleAPI.Models.Models;
using ExampleAPI.Models.ModelsDto;
using ExampleAPI.Connector;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace ExampleAPI.Controllers
{
    [RoutePrefix("api/v1")]
    public class EmployeeController : ApiController
    {
        /// <summary>
        /// Get Employees
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("employee")]
        [ResponseType(typeof(Response<List<EmployeeDto>>))]
        public async Task<IHttpActionResult> Get()
        {
            // Request API
            IServiceConnector connector = new ServiceConnector("http://dummy.restapiexample.com/api/v1/");
            Response<List<EmployeeDto>> employeesResponse = await connector.GetAsync<List<EmployeeDto>>("employees");

            // Prepare response
            object result = employeesResponse.Data;
            HttpResponseMessage responseMessage = Request.CreateResponse(HttpStatusCode.OK, result);

            return ResponseMessage(responseMessage);
        }

        /// <summary>
        /// Get Employee
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet, Route("employee/{id}")]
        [ResponseType(typeof(Response<EmployeeDto>))]
        public async Task<IHttpActionResult> Get([Required]int id)
        {
            // Request API
            IServiceConnector connector = new ServiceConnector("http://dummy.restapiexample.com/api/v1/");
            Response<EmployeeDto> employeesResponse = await connector.GetAsync<EmployeeDto>(string.Format("employee/{0}", id));

            // Prepare response
            object result = employeesResponse.Data;
            HttpResponseMessage responseMessage = Request.CreateResponse(HttpStatusCode.OK, result);

            return ResponseMessage(responseMessage);
        }

        /// <summary>
        /// Create Employee
        /// </summary>
        /// <param name="requestBody"></param>
        /// <returns></returns>
        [HttpPost, Route("employee/create")]
        [ResponseType(typeof(Response<EmployeeCreateDto>))]
        public async Task<IHttpActionResult> Create([FromBody]EmployeeCreateDto requestBody)
        {
            // Prepare request body
            StringContent content = new StringContent(JsonConvert.SerializeObject(requestBody), Encoding.UTF8, "application/json");

            // Request API
            IServiceConnector connector = new ServiceConnector("http://dummy.restapiexample.com/api/v1/");
            Response<EmployeeCreateDto> employeesResponse = await connector.PostAsync<EmployeeCreateDto>("create", content);

            // Prepare response
            object result = employeesResponse.Data;
            HttpResponseMessage responseMessage = Request.CreateResponse(HttpStatusCode.OK, result);

            return ResponseMessage(responseMessage);
        }
        
        /// <summary>
        /// Get Employee Average Age
        /// </summary>
        /// <returns></returns>
        [HttpGet, Route("employee/average")]
        [ResponseType(typeof(Response<EmployeeAverageDto>))]
        public async Task<IHttpActionResult> Average()
        {
            // Request API
            IServiceConnector connector = new ServiceConnector("http://dummy.restapiexample.com/api/v1/");
            Response<List<EmployeeDto>> employeesResponse = await connector.GetAsync<List<EmployeeDto>>("employees");

            // Prepare response
            object result = new EmployeeAverageDto()
            {
                Average = employeesResponse.Data.Average(x => x.EmployeeAge)
            };
            HttpResponseMessage responseMessage = Request.CreateResponse(HttpStatusCode.OK, result);

            return ResponseMessage(responseMessage);
        }
    }
}
