﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace ExampleAPI.Models.Models
{
    public class Response<T>
    {
        /// <summary>
        /// HttpStatusCode Number
        /// </summary>
        [JsonProperty(PropertyName = "status")]
        public string Status { get; set; }

        /// <summary>
        /// Response Data
        /// </summary>
        [JsonProperty(PropertyName = "data")]
        public T Data { get; set; }

        /// <summary>
        /// HttpStatusCode Message and Exception Message
        /// </summary>
        [JsonProperty(PropertyName = "message")]
        public string Message { get; set; }

        public void SetStatus(HttpStatusCode statusCode)
        {
            Status = Convert.ToInt32(statusCode).ToString();
            Message = statusCode.ToString();
        }

        public void SetStatus(HttpStatusCode statusCode, Exception error)
        {
            Status = Convert.ToInt32(statusCode).ToString();
            Message = statusCode.ToString() + ": " + error.ToString();
        }
    }
}