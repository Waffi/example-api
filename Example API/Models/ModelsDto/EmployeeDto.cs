﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ExampleAPI.Models.ModelsDto
{
    public class EmployeeDto
    {
        /// <summary>
        /// Employee Id
        /// </summary>
        [JsonProperty(PropertyName = "id")]
        public string Id { get; set; }

        /// <summary>
        /// Employee Name
        /// </summary>
        [JsonProperty(PropertyName = "employee_name")]
        public string EmployeeName { get; set; }

        /// <summary>
        /// Employee Salary
        /// </summary>
        [JsonProperty(PropertyName = "employee_salary")]
        public int EmployeeSalary { get; set; }

        /// <summary>
        /// Employee Age
        /// </summary>
        [JsonProperty(PropertyName = "employee_age")]
        public int EmployeeAge { get; set; }

        /// <summary>
        /// Employee Profile Image
        /// </summary>
        [JsonProperty(PropertyName = "profile_image")]
        public string ProfileImage { get; set; }
    }

    public class EmployeeCreateDto
    {        
        /// <summary>
        /// Employee Name
        /// </summary>
        [JsonProperty(PropertyName = "name")]
        public string Name { get; set; }

        /// <summary>
        /// Employee Salary
        /// </summary>
        [JsonProperty(PropertyName = "salary")]
        public string Salary { get; set; }
        
        /// <summary>
        /// Employee Age
        /// </summary>
        [JsonProperty(PropertyName = "age")]
        public string Age { get; set; }
    }

    public class EmployeeAverageDto
    {
        /// <summary>
        /// Employee Average Age
        /// </summary>
        [JsonProperty(PropertyName = "average")]
        public double Average { get; set; }
    }
}